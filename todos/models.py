from django.db import models
from django.forms import ModelForm
from django import forms

class TodoList(models.Model):
    name=models.CharField(max_length=100)
    created_on=models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

class TodoItem(models.Model):
    task=models.CharField(max_length=100)
    due_date=models.DateTimeField(null=True, blank=True)
    is_completed=models.BooleanField(default=False)

    list = models.ForeignKey(TodoList,related_name="items",on_delete=models.CASCADE)


class TodoForm(ModelForm):
    class Meta:
        model = TodoList
        fields = [
            "name"
        ]

class TodoFormItem(ModelForm):
    class Meta:
        model = TodoItem
        fields = [
            "task",
            "due_date",
            "is_completed",
            "list"
        ]
