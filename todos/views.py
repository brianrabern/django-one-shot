from django.shortcuts import render, get_object_or_404,redirect
from todos.models import TodoList, TodoItem, TodoForm, TodoFormItem

# Create your views here.

def todo_list(request):
    tlists = TodoList.objects.all()

    context = {"tlists": tlists}

    return render(request,"todos/todos.html",context)


def todo_detail(request,id):
    detail = get_object_or_404(TodoList,id=id)
    context={"list": detail}
    return render(request, "todos/details.html", context)


def create_list(request):
    if request.method == "POST":
        tlist=TodoForm(request.POST)
        if tlist.is_valid():
            x=tlist.save()
            return redirect('todo_list_detail',x.id)

    else:
        tlist=TodoForm()
    context={"tlist": tlist}

    return render(request,"todos/create.html", context)



def edit_list(request,id):
    thing = get_object_or_404(TodoList,id=id)
    if request.method == "POST":
        tlist = TodoForm(request.POST, instance=thing)
        if tlist.is_valid():
            tlist.save()
            return redirect("todo_list_detail", id=id)
    else:
        tlist = TodoForm(instance=thing)
    context = {
        "tofu": thing,
        "tlist": tlist}
    return render(request, "todos/edit.html", context)


def delete_list(request,id):
    thing = get_object_or_404(TodoList,id=id)
    if request.method == "POST":
        thing.delete()
        return redirect("todo_list_list")
    else:
        context = {"tofu": thing}
        return render(request, "todos/delete.html", context)


def add_item(request):
    if request.method == "POST":
        item =TodoFormItem(request.POST)
        if item.is_valid():
            item.save()
            task = item.save()
            list_id=task.list.id
            return redirect("todo_list_detail",list_id)

    else:
        item =TodoFormItem()
    context={"item": item}

    return render(request,"todos/item/create.html", context)






def edit_item(request,id):
    item = get_object_or_404(TodoItem,id=id)
    if request.method == "POST":
        thing = TodoFormItem(request.POST, instance=item)
        if thing.is_valid():
            thing.save()
            task = thing.save()
            list_id=task.list.id
            return redirect("todo_list_detail", list_id)
    else:
        thing = TodoFormItem(instance=item)
    context = {
        "thing": thing,
        "item": item}
    return render(request, "todos/item/edit.html", context)
